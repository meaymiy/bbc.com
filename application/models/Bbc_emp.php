<?php 
class Bbc_emp extends CI_Model {
    public function get_allemp()
    {
        $query = $this->db->get('emp_info');
        return $query->result();
    }
    public function get_allemp_jobordept($id = 1)
    {   
        $wheres;
        $id == 1 ? $wheres = 'job' : $wheres = 'dept';  
        $query = $this->db->select($wheres)->distinct()->get('emp_info');
        return $query->result();
    }
    public function insert_emp($data)
    {
        if($this->db->insert('emp_info', $data)){
            return 1;
        }
        else{
            return 0;
        }
    }
    public function update_emp_info($data,$ids)
    {
       $this->db->update('emp_info', $data, array('id' => $ids));
       return 1;
   }
   public function del_emp_info($ids)
    {

       $this->db->where('id',$ids)->delete('emp_info');
       return 1;
   }
}