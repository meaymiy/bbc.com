<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bbc_controller extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	public function index()
	{
		$this->load->database();
		$this->load->dbutil();
		$this->load->helper('url');
		$this->load->library('parser');
		$this->load->model('bbc_emp');
		$romark = $this->bbc_emp->get_allemp();
		$jobs = $this->bbc_emp->get_allemp_jobordept();
		$depts = $this->bbc_emp->get_allemp_jobordept(0);
		$data = array(
			'title'=>'BBC NEWS',
			'author'=>'meaymiy',
			'emps'=>$romark,
			'jobs'=>$jobs, 
			'depts'=>$depts,
		);
		$this->parser->parse('templates/head',$data);
		$this->load->view('templates/navbar');
		$this->parser->parse('bbc_index',$data);
		$this->load->view('templates/footer');
	}
	public function create(){
		$this->load->database();
		$this->load->dbutil();
		$this->load->model('bbc_emp');
		$insertnewemp = array(
			'first_name'	=>	$this->input->post('fname'),
			'last_name'		=>	$this->input->post('lname'),
			'email'			=>	$this->input->post('email'),
			'dob'			=>	$this->input->post('dob'),
			'gender'		=>	$this->input->post('sex'),
			'job'			=>	$this->input->post('jobs'),
			'dept'			=>	$this->input->post('depts'),
		);
		// var_dump($insertnewemp);
		if($this->bbc_emp->insert_emp($insertnewemp)){
			redirect('./', 'refresh');
		}
		else{
			echo "nay";
		}
	}
	public function update_emp(){
		$this->load->database();
		$this->load->dbutil();
		$this->load->model('bbc_emp');
		$id=$this->input->post('id_up'); 
		$update_data = array(
			'first_name'	=>	$this->input->post('fname_up'),
			'last_name'		=>	$this->input->post('lname_up'),
			'email'			=>	$this->input->post('email_up'),
			'dob'			=>	$this->input->post('dob_up'),
			'gender'		=>	$this->input->post('sex_up'),
			'job'			=>	$this->input->post('jobs_up'),
			'dept'			=>	$this->input->post('depts_up'),
		);
		// var_dump($update_data,$id);
		$this->bbc_emp->update_emp_info($update_data,$id) ? redirect('./', 'refresh') : error_reporting(E_ALL);
	}
	public function tester()
	{
		$this->load->database();
		$this->load->dbutil();
		$this->load->model('bbc_emp');
		$duper = $this->bbc_emp->get_allemp_jobordept(0);
		var_dump($duper);
	}
	public function delete($id=0){
		$this->load->database();
		$this->load->dbutil();
		$this->load->model('bbc_emp');
		
		if($this->bbc_emp->del_emp_info($id)){
			echo $id;
		}
		else{
			echo error_reporting(E_ALL);
		}

	}
	// public function about()
	// {
	// 	$this->load->helper('url');	
	// 	$this->load->view('welcome_message');
	// }
	// public function services()
	// {
	// 	$this->load->view('templates/head');
	// 	$this->load->view('templates/navbar');
	// 	echo "services";
	// 	$this->load->view('templates/footer');
	// }
	// public function prices(){
	// 	$this->load->view('templates/head');
	// 	$this->load->view('templates/navbar');
	// 	echo "prices";
	// 	$this->load->view('templates/footer');
	// }
	// public function contact()
	// {
	// 	$this->load->view('templates/head');
	// 	$this->load->view('templates/navbar');
	// 	echo "contact";
	// 	$this->load->view('templates/footer');
	// }
	// public function join_us()
	// {
	// 	$this->load->view('templates/head');
	// 	$this->load->view('templates/navbar');
	// 	echo "join us";
	// 	$this->load->view('templates/footer');
	// }
	public function fourohfour(){
		$this->load->view('templates/head');
		$this->load->view('templates/navbar');
		echo "<h1>Jowa not Found</h1>";
		$this->load->view('templates/footer');
	}
}
