<div class="jumbotron">
	<h1 class="display-3">{title}</h1>
	<p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
	<hr class="m-y-md">
	<p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
	<p class="lead">
		<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
	</p>
</div>
<div class="container">
	<button class="mt-5 ml-auto mb-3 btn btn-success text-uppercase" data-toggle="modal" data-target="#newemp">new</button>
	<div class="row">
		<div class="col-12">
			<table id="myTable" class="table table-responsive table-bordered table-striped" >
				<thead>
					<th>ID</th>
					<th>Last Name</th>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Job</th>
					<th>Department</th>
					<th><img src="https://img.icons8.com/color/48/000000/settings-3--v1.png"></th>
				</thead>
				<tbody>
					{emps}
					<tr>
						<td>{id}</td>
						<td>{first_name}</td>
						<td>{last_name}</td>
						<td>{email}</td>
						<td>{job}</td>
						<td>{dept}</td>
						<td><button onclick='update({"id":{id}, "first_name":"{first_name}", "last_name":"{last_name}", "email":"{email}","gender":"{gender}", "dob":"{dob}", "job":"{job}", "dept":"{dept}"})' data-toggle="modal" data-target="#empupdate" class="btn-primary btn mr-auto ml-auto" ><img src="https://img.icons8.com/wired/12/ffffff/edit.png"></button></td> </tr>
						{/emps}
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="container-fluid navbar-bbc">
		<div class="row mt-5 p-5">
			<p class="text-center text-white">copyright 2019-2022</p>
		</div>
	</div>
	<!-- Modal here -->
	<div class="modal fade" id="newemp">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header navbar-bbc text-white">
					<h4 class="modal-title">New Employee</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body " >
					<form action="./create" method="post" id="reg">
						<div class="form-group">
							<label for="fname">First Name</label>
							<input class="form-control" required="" type="text" name="fname" id="fname">
						</div>
						<div class="form-group">
							<label for="lname">Last Name</label>
							<input class="form-control" required="" type="text" name="lname" id="lname">
						</div>
						<div class="form-group">
							<label for="email">email</label>
							<input class="form-control" type="text" name="email" id="email">
						</div>
						<label >Sex</label>
						<br>
						<div class="form-check-inline">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="sex" value="male">male
							</label>
						</div>
						<div class="form-check-inline">
							<label class="form-check-label">
								<input type="radio" class="form-check-input" name="sex" value="female">female
							</label>
						</div>
						<div class="form-group">
							<label for="dob">date of birth</label>
							<input class="form-control" type="date" name="dob" id="dob">
						</div>
						<div class="form-group">
							<label for="sel1">Job</label>
							<input name="jobs"   list="encodings" value="" class="col-sm-6 custom-select custom-select-sm">
							<datalist id="encodings">
								{jobs}
								<option value="{job}">
									{/jobs}
								</datalist>
							</div>
							<div class="form-group">
								<label for="sel1">Department</label>
								<input  name="depts"  list="depts" value="" class="col-sm-6 custom-select custom-select-sm">
								<datalist id="depts">
									{depts}
									<option value="{dept}">
										{/depts}
									</datalist>
								</div>
							</form>
						</div>
						<!-- Modal footer -->
						<div class="modal-footer navbar-bbc">
							<button type="submit" class="btn btn-success" form="reg" type="submit" >Add</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal here -->
			<div class="modal fade" id="empupdate">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header navbar-bbc text-white">
							<h4 class="modal-title">Update info</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<!-- Modal body -->
						<div class="modal-body " >
							<form action="./update" method="post" id="update">
								<input type="hidden"  name="id_up" id="id_up">
								<div class="form-group">
									<label for="fname">First Name</label>
									<input class="form-control" required="" type="text" name="fname_up" id="fname_up">
								</div>
								<div class="form-group">
									<label for="lname">Last Name</label>
									<input class="form-control" required="" type="text" name="lname_up" id="lname_up">
								</div>
								<div class="form-group">
									<label for="email">email</label>
									<input class="form-control" type="text" name="email_up" id="email_up">
								</div>
								<label >Sex</label>
								<br>
								<div class="form-check-inline">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" id="sexm" name="sex_up" value="male">male
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" id="sexf" name="sex_up" value="female">female
									</label>
								</div>
								<div class="form-group">
									<label for="dob">date of birth</label>
									<input class="form-control" type="date" name="dob_up" id="dob_up">
								</div>
								<div class="form-group">
									<label for="sel1">Job</label>
									<input name="jobs_up"   list="encodings" value="" id="job_up" class="col-sm-6 custom-select custom-select-sm">
									<datalist id="encodings">
										{jobs}
										<option value="{job}">
											{/jobs}

											</datalist>
										</div>
										<div class="form-group">
											<label for="sel1">Department</label>
											<input  name="depts_up"  list="depts" value="" id="dept_up" class="col-sm-6 custom-select custom-select-sm">
											<datalist id="depts">
												
													{depts}
													<option value="{dept}">
														{/depts}
													</datalist>
												</div>
											</form>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer navbar-bbc text-white">
											<a  class="btn btn-dan=ger mr-auto" onclick="del()" id="delete" >Delete</a>
											<button type="submit" class="btn btn-success" form="update" type="submit" >Update</button>
											<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
							<script>
								
								var emp_arr_global; 
								function update(emp_arr){
									emp_arr_global = emp_arr;
				// console.log(emp_arr);
				document.getElementById('fname_up').placeholder = emp_arr.first_name
				document.getElementById('lname_up').placeholder = emp_arr.last_name
				document.getElementById('email_up').placeholder = emp_arr.email
				if(emp_arr.gender=='male'){
					document.getElementById('sexm').checked = true
				}
				else{
					document.getElementById('sexf').checked = true
				}
				document.getElementById('dob_up').placeholder = emp_arr.dob
				document.getElementById('job_up').placeholder = emp_arr.job
				document.getElementById('dept_up').placeholder = emp_arr.dept
				document.getElementById('id_up').value= emp_arr.id
				document.getElementById('fname_up').value = emp_arr.first_name
				document.getElementById('lname_up').value = emp_arr.last_name
				document.getElementById('email_up').value = emp_arr.email
				document.getElementById('dob_up').value=new String(emp_arr.dob);
				document.getElementById('job_up').value=emp_arr.job
				document.getElementById('dept_up').value=emp_arr.dept
			}
			function del(){
				Swal.fire({
					title: 'Are you sure you want to delete #:'+emp_arr_global.id+' data?',
					text: "You won't be able to revert this!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							type: "POST",
							url: './delete/'+emp_arr_global.id,
							success: function(data){
								Swal.fire(
									'Deleted!',
									'Employee #:'+data+' has been deleted.',
									'success'
									).then((result) => {
										if (result.value) {
											window.location.href = "./"
										}
									})
								},
								error: function() {
									Swal.fire({
										icon: 'error',
										title: 'Oops...',
										text: 'Something went wrong!',
										footer: '<a href>Why do I have this issue?</a>'
									})
								}
							});
					}
				})
			}
		</script>