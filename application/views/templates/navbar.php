<?php 
$navs = array('about' => '#',
              'services' => '#',
              'prices' => '#',
              'contact' => '#',
              'join us' => '#',
               );
// $navs = array('about' => '/about',
//               'services' => '/services',
//               'prices' => '/prices',
//               'contact' => '/contact',
//               'join us' => '/join_us',
//                );


 ?>
<nav class="navbar navbar-expand-sm bg-dark  navbar-bbc mb-1">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">
    <img src="https://clipground.com/images/bbc-news-png-10.png" alt="logo" style="width:80px;">
  </a>
  
  <!-- Links -->
  <ul class="navbar-nav">
    <?php 
    $linkatt =array( 'class'=>'nav-link text-uppercase',
    );
      foreach ($navs as $key => $nav) {
        # code...
        ?>
        <li class="nav-item">
        	<?php echo anchor( $nav,$key, $linkatt ); ?>
         
        </li>
        <?php
      }
     ?>
    
  </ul>
</nav>