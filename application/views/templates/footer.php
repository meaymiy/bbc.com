<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <!--  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
   <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/r-2.2.3/datatables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
    	$(document).ready( function () {
        $.ajax({
              type: "GET",
              url: 'https://sv443.net/jokeapi/category/any',
              success: function(data){
                // localStorage.setItem('key_name','value to save') set variable value in localstorage
                // localStorage.getItem('key_name') get value in localstorage
                // localStorage.removeItem('key_name') removes value in localstorage
                // sessionStorage.setItem('key_name','value to save') set variable value in sessionstorage
                // sessionStorage.getItem('key_name') get value in sessionstorage
                // sessionStorage.removeItem('key_name') removes value in sessionstorage
                Swal.fire(
                  data.setup,
                  data.delivery,
                  'question'
                  )
                },
                error: function() {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: '<a href>Why do I have this issue?</a>'
                  })
                }
              });
       
    		$('#myTable').DataTable( {
    			dom: 'Bfrtip',
    			buttons: [
    			'copy','pdf', 'excel', 
    			]
    		});
    	} );
    </script>
  </body>
  
</html>